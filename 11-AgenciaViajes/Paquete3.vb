﻿Public Class Paquete3
    Inherits TemplatePaqueteTuristico

    Protected Overrides Sub RealizarPrimerActividad()
        Console.WriteLine("Visitando Santa Clara...")
    End Sub

    Protected Overrides Sub RealizarSegundaActividad()
        Console.WriteLine("Visitando el faro...")
    End Sub
End Class
