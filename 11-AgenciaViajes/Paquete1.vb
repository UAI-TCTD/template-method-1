﻿Public Class Paquete1
    Inherits TemplatePaqueteTuristico

    Protected Overrides Sub RealizarPrimerActividad()
        Console.WriteLine("Visitando la lobería...")
    End Sub

    Protected Overrides Sub RealizarSegundaActividad()
        Console.WriteLine("Visitando el faro...")
    End Sub
End Class
