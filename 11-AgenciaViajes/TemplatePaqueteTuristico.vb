﻿Public MustInherit Class TemplatePaqueteTuristico

    Public Sub EjecutarPaquete()
        ServirDesayuno()
        RealizarPrimerActividad()
        ServirAlmuerzo()
        RealizarSegundaActividad()
    End Sub

    Private Sub ServirDesayuno()
        Console.WriteLine("Sirviendo desayuno!")
    End Sub

    Private Sub ServirAlmuerzo()
        Console.WriteLine("Sirviendo almuerzo!")
    End Sub

    Protected MustOverride Sub RealizarPrimerActividad()
    Protected MustOverride Sub RealizarSegundaActividad()



End Class
